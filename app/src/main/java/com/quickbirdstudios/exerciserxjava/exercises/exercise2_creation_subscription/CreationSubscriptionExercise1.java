package com.quickbirdstudios.exerciserxjava.exercises.exercise2_creation_subscription;

import android.annotation.SuppressLint;

import com.quickbirdstudios.exerciserxjava.util.ExecutionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class CreationSubscriptionExercise1 {
    public static void main(String[] args) {
        printMultipliedNumbers(Arrays.asList(2, 4, 6), 2);
    }

    @SuppressLint("CheckResult")
    private static void printMultipliedNumbers(List<Integer> numbers, int times) {
        // TODO 1: a) create an Observable from the "numbers" argument
//        --- now apply operators to the observable:
        // TODO 1: b) multiply each "number" in the Observable with "times"
        // TODO 1: c) map each multiplied number to a String
        // TODO 1: d) subscribe to the Observable and print out each value that is emitted
        // example: for numbers = 1,2,3 and times = 2 --> output = "2,4,6"



    }
}
