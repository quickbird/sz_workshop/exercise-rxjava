package com.quickbirdstudios.exerciserxjava.exercises.exercise2_creation_subscription;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class CreationSubscriptionExercise3 {
    public static void main(String[] args) {
        System.out.println("vJust-Observable***");
        printResult(Observable.just("ELEMENT"));
        System.out.println("***Error-Observable***");
        printResult(Observable.error(new IllegalStateException("ERROR")));
        System.out.println("***Empty-Observable***");
        printResult(Observable.empty());
    }

    private static void printResult(Observable<String> result) {
//        TODO 3 subscribe to the result
//        TODO 3 print "onNext <element>" for each element
//        TODO 3 print "onError <error>" when error occurs
//        TODO 3 print "onComplete" for when observable completes



    }
}
