package com.quickbirdstudios.exerciserxjava.exercises.exercise4_operators_part2;

import com.quickbirdstudios.exerciserxjava.model.Country;
import com.quickbirdstudios.exerciserxjava.providers.CountriesTestProvider;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

public class AdvancedCountriesServiceTest {

    private AdvancedCountriesService countriesService;
    private List<Country> allCountries;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    @Before
    public void setUp() {
        countriesService = new AdvancedCountriesService();
        allCountries = CountriesTestProvider.countries();
    }

    @Test
    public void rx_ListPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty_When_NoTimeout() {
        FutureTask<List<Country>> futureTask = new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(100);
            return allCountries;
        });
        new Thread(futureTask).start();
        TestObserver<Country> testObserver = countriesService
                .listPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty(futureTask)
                .test();
        List<Country> expectedResult = CountriesTestProvider.countriesPopulationMoreThanOneMillion();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValueSet(expectedResult);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_ListPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty_When_Timeout() {
        FutureTask<List<Country>> futureTask = new FutureTask<>(() -> {
            TimeUnit.HOURS.sleep(1);
            return allCountries;
        });
        new Thread(futureTask).start();
        TestObserver<Country> testObserver = countriesService
                .listPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty(futureTask)
                .test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoValues();
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_sumPopulationOfCountries() {
        // hint: use "map" operator
        TestObserver<Long> testObserver = countriesService
                .sumPopulationOfCountries(Observable.fromIterable(allCountries), Observable.fromIterable(allCountries))
                .test();
        testObserver.assertResult(CountriesTestProvider.sumPopulationOfAllCountries()
                + CountriesTestProvider.sumPopulationOfAllCountries());
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_areEmittingSameSequences_Positive() {
        // hint: use "sequenceEqual" operator
        TestObserver<Boolean> testObserver = countriesService
                .areEmittingSameSequences(Observable.fromIterable(allCountries), Observable.fromIterable(allCountries))
                .test();
        testObserver.assertResult(true);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_areEmittingSameSequences_Negative() {
        List<Country> allCountriesDifferentSequence = new ArrayList<>(allCountries);
        Collections.swap(allCountriesDifferentSequence, 0, 1);
        TestObserver<Boolean> testObserver = countriesService
                .areEmittingSameSequences(
                        Observable.fromIterable(allCountries),
                        Observable.fromIterable(allCountriesDifferentSequence))
                .test();
        testObserver.assertResult(false);
        testObserver.assertNoErrors();
    }
}
