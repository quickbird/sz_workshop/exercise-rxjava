package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator;

import android.arch.lifecycle.ViewModel;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserService;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.utils.DependencyInjector;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Malte Bucksch on 10/04/2018.
 */

public class TranslatorViewModelImpl extends ViewModel implements TranslatorViewModel {
    private UserService userService = DependencyInjector.injectUserService();

    private final Subject<String> inputEnglishText = BehaviorSubject.createDefault("");
    private final Subject<Boolean> inputSaveTrigger = PublishSubject.create();
    private final Subject<Boolean> inputLogoutTrigger = PublishSubject.create();


    // --- inputs
    @Override
    public Subject<String> inputEnglishText() {
        return inputEnglishText;
    }

    @Override
    public Subject<Boolean> inputSaveTrigger() {
        return inputSaveTrigger;
    }

    @Override
    public Subject<Boolean> inputLogoutTrigger() {
        return inputLogoutTrigger;
    }

    // --- outputs
    @Override
    public Observable<String> outputGermanText() {
        return inputEnglishText()
                .map(TranslatorEngine::translateToGerman);
    }

    @Override
    public Observable<Boolean> outputIsSavingAllowed() {
        return inputEnglishText()
                .map(english -> !english.isEmpty());
    }

    @Override
    public Observable<String> outputSavedGermanTranslation() {
        return inputSaveTrigger()
                .withLatestFrom(outputGermanText(), (trigger, germanText) -> germanText);
    }

    @Override
    public Observable<Boolean> outputLoggedOut() {
        return inputLogoutTrigger()
                .flatMap(v -> userService.logout());
    }
}
