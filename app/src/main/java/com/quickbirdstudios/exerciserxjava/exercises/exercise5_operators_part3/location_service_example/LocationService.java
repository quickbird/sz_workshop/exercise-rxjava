package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3.location_service_example;

import com.quickbirdstudios.exerciserxjava.model.Point;

/**
 * Created by Malte Bucksch on 19/06/2018.
 */
class LocationService {
    public void addOnLocationUpdateListener(OnLocationUpdateListener onLocationUpdateListener) {
        // mocked
        new Thread(() -> {
            while (onLocationUpdateListener != null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                onLocationUpdateListener.onLocationUpdated(
                        new Point(Math.random() * 10000d, Math.random() * 10000));
            }
        }).start();
    }

    interface OnLocationUpdateListener {
        void onLocationUpdated(Point point);
    }
}
