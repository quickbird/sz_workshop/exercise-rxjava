package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3;

import com.quickbirdstudios.exerciserxjava.model.Cafe;
import com.quickbirdstudios.exerciserxjava.model.CafePicture;

import org.junit.Test;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class RxNetworkClientTest {
    private RxNetworkClient rxNetworkClient = new RxNetworkClient();

    @Test
    public void testSearchNearbyCafePlaces_success() {
        rxNetworkClient.searchNearbyCafePlaces("test")
                .test()
                .assertValue(cafes -> cafes.size() == 3)
                .assertComplete();
    }

    @Test
    public void testSearchNearbyCafePlaces_failed() {
        rxNetworkClient.searchNearbyCafePlaces(null)
                .test()
                .assertError(Throwable.class);
    }

    @Test
    public void testFetchCafePlacePicture_success() {
        rxNetworkClient.fetchCafePlacePicture(new Cafe("Starbucks",2.3f))
                .test()
                .assertValueCount(1)
                .assertComplete();
    }

    @Test
    public void testFetchCafePlacePicture_failed() {
        rxNetworkClient.fetchCafePlacePicture(null)
                .test()
                .assertError(Throwable.class);
    }
}